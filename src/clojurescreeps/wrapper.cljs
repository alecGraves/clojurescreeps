;
;  Wrapper for screeps api calls
;     es ma raison d'e^tre
;
(ns clojurescreeps.wrapper)

(defn c "js->clj"  [x]
  (js->clj x))

(defn j "clj->js" [x]
  (clj->js x))

(defn spawnCreep [^js/StructureSpawn spawn newCreepName]
  (println "Spawning creep " newCreepName "...")
  (.spawnCreep spawn
               (j ["work", "carry", "move"])
               (j newCreepName)))

(defn getPosition [^js/RoomPosition position]
  (c [(.-x position) (.-y position)]))

(defn clojureCreep
  "Convert a screep into a nice little map structure."
  [^js/Creep jsCreep]
  (let [store (.-store jsCreep) body (c (.-body jsCreep))]
    {:name (c (.-name jsCreep))
     :body (map (fn [b] (b "type")) body)
     :storageMax (c (.getCapacity store))
     :storageFree (c (.getFreeCapacity store))
     :energy (c (.-energy store))
     :position (getPosition (.-pos jsCreep))
     :roomName (c (.. jsCreep -pos -roomName))}))

(defn clojureSource [^js/Source jsSource]
    {:position (getPosition (.-pos jsSource))
    :roomName (.. jsSource -room -name)
    :energy (.-energy jsSource)
    :energyCapacity (.-energyCapacity jsSource)
    :ticksToRegeneration (.-ticksToRegeneration jsSource)})

(defn find [^js/Room room key]
  (c (.find room key)))

(defn findSources [^js/Room room]
  (map clojureSource (find room 105)))
