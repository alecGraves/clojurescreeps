;
;  Creep AI logic
;     es ma raison d'e^tre
;
(ns clojurescreeps.creep
  (:require [clojurescreeps.math :as math]))

(defn full [creep]
  "Is this creep carrying max capacity?"
  (= (creep :storageFree) 0))

(defn harvest
  "Makes a creep harvest a source and return it to the thing." [creep energyPosition moveFn harvestFn depositFn] nil)

