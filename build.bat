@echo off

echo Building clojurescreeps.js then removing the shebang ('#')

java -cp "cljs.jar;src" cljs.main -co build_opts.edn -c && more +1 clojurescreeps.js > tmp.js && del clojurescreeps.js && rename tmp.js clojurescreeps.js

COPY clojurescreeps.js %USERPROFILE%\AppData\Local\Screeps\scripts\screeps.com\clojurescreeps
COPY main.js %USERPROFILE%\AppData\Local\Screeps\scripts\screeps.com\clojurescreeps
